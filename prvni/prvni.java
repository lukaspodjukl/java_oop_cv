package prvni;

class Prvni {
    public static void main(String[] args) {
        Student s1 = new Student(20, 1000);
        s1.writeInfo();
        Accountant a1 = new Accountant(30, 20000);
        a1.writeInfo();
        Teacher t1 = new Teacher(40, 35000, 55);
        t1.writeInfo();

        Student s2 = new Student();
        s2.writeInfo();

        System.out.println("Počet osob: " + Person.getCount());

        System.out.println("\n");
        System.out.println(s1);
        System.out.println(t1);
    }
}

class Person {
    private int age;
    protected static int count = 0;

    public int getAge() {
        return age;
    }

    public void setAge(int value) {
        age = value;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int value) {
        count = value;
    }

    public Person() {
        count++;
    }

    public Person(int age) {
        this.age = age;
        count++;
    }

    public void writeInfo() {
        System.out.println("Věk je " + getAge());
        System.out.println("Pořadí instance osoby: " + count);
    }

    public String toString() {
        return "Věk: " + getAge() + "\t" + "instance: " + getCount();
    }

}

abstract class Employee extends Person {
    public int salary;

    // public Employee() { }
    public Employee(int age, int salary) {
        super(age);
        this.salary = salary;
    }

    public void writeInfo() {
        super.writeInfo();
        System.out.println("Plat: " + salary);
    }
}

class Student extends Person {
    public int scholarship;

    public Student() {
    }

    public Student(int age, int scholarship) {
        super(age);
        // this.age = age;
        this.scholarship = scholarship;
    }

    public void writeInfo() {
        System.out.println("Student...");
        super.writeInfo();
        System.out.println("Stipendium: " + scholarship);
        System.out.println("\n");
    }

    public String toString() {
        return "Student" + "\n" + super.toString() + "\n" + "Stipendium " + scholarship + "\n";
    }
}

class Accountant extends Employee {

    public Accountant(int age, int salary) {
        super(age, salary);
        // this.age = age;
        // this.salary = salary;
    }

    public void writeInfo() {
        System.out.println("Účetní...");
        super.writeInfo();
        System.out.println("\n");
    }
}

class Teacher extends Employee {
    public int teachingTime;

    public Teacher(int age, int salary, int teachingTime) {
        super(age, salary);
        // this.age = age;
        // this.salary = salary;
        this.teachingTime = teachingTime;
    }

    public void writeInfo() {
        System.out.println("Učitel...");
        super.writeInfo();
        System.out.println("Doba výuky: " + teachingTime);
        System.out.println("\n");
    }

}
