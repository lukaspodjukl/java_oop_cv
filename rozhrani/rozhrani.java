package rozhrani;

//import java.util.*;     //pro kolekce atd.

class Rozhrani {
    public static void main(String[] args) {
            //Dog p1 = new Dog("Rex");
            //Cat k1 = new Cat("Ferda");
            //Turtle z1 = new Turtle("Maja");

            //plnění pole objektů:
            //Animal[] animals = new Animal[3];     //s přesným rozměrem pole
            //Animal[] animals = new Animal[Animal.countOfAnimals];    //rozměr dle počtu již vytvořených instancí
            //animals[0] = p1;
            //animals[1] = k1;
            //animals[2] = z1;

            //druhá varianta - deklarace pole s již vytvořenými instancemi v jednom řádku (není třeba countOfAnimals)
            //Animal[] animals = new Animal[] { p1, k1, z1 };   //popř.: Animal[] animals = { p1, k1, z1 };

            //třetí varianta - deklarace pole včetně vytváření nových instancí v jednom řádku
            Animal[] animals = { new Dog("Marcel"), new Dog("Azor"), new Dog("Speedy"), new Cat("Maggie"), new Cat("Micka"), new Turtle("Milfka") };

            // for (int i = 0; i < animals.length; i++) {      //výpis pole objektů (for)
            //    System.out.println(animals[i].name + " je " + animals[i].getClass().getName());
            // }

            //System.out.println(((Dog)animals[2]).sound());    //Dog pes = (Dog)animals[2];    pes.sound());
            //System.out.println(((Cat)animals[3]).sound());    //Cat kocka = (Cat)animals[3];  kocka.sound();

            for (int i = 0; i < animals.length; i++) { //výpis pole/kolekce objektů (for)
                // if (animals[i] instanceof Dog) {
                //    System.out.println(((Dog)animals[i]).sound());
                // }
                // else if (animals[i] instanceof Cat) {
                //    System.out.println(((Cat)animals[i]).sound());
                // }
                // else if (animals[i] instanceof Turtle) {
                //    System.out.println("bez zvuku");
                // }
                // if (animals[i].getClass().getName() == "Dog") {
                //    System.out.println(((Dog)animals[i]).sound());
                // }
                //ternární výraz (ve for), u želvy se vrátí prázdný řetězec:
                System.out.println(animals[i] instanceof Dog ? ((Dog)animals[i]).sound() : animals[i] instanceof Cat ? ((Cat)animals[i]).sound() : "");
            }
            
            /*
            ArrayList animalsCollect = new ArrayList();   //deklarace kolekce a plnění...
            animalsCollect.add(new Dog("Marcel"));
            animalsCollect.add(new Dog("Azor"));
            animalsCollect.add(new Dog("Speedy"));
            animalsCollect.add(new Cat("Maggie"));
            animalsCollect.add(new Cat("Micka"));
            animalsCollect.add(new Turtle("Milfka"));
            //nebo rovnou: List<Animal> animalsCollect = new List<Animal>() { new Dog("Marcel"), new Dog("Azor"), new Dog("Speedy"), new Cat("Maggie"), new Cat("Micka"), new Turtle("Milfka") };       

            for (Animal a : animalsCollect) {     //výpis pole/kolekce objektů (foreach)    //"a" jako item of animals...
                if (a instanceof Dog || a instanceof Cat) {
                    System.out.println(a.name + " je " + a.getClass().getName() + " a dělá " + ((a instanceof Dog) ? ((Dog)a).sound() : ((Cat)a).sound()));
                }
                else {
                    System.out.println(a.name + " je " + a.getClass().getName() + " a nedělá žádný zvuk");
                }
            }
            for (int i = 0; i < animalsCollect.size(); i++) {  //další varianta: výpis kolekce objektů přes for...
                //zde místo "a" viz výše dát akorát "animalsCollect[i]" a je to podobné...
            }
            */

            // ISoundable[] animalsInterf = { new Dog("Marcel"), new Dog("Azor"), new Dog("Speedy"), new Cat("Maggie"), new Cat("Micka") /*, new Turtle("Milfka") - nelze! */ };
            // for (ISoundable aI : animalsInterf) {
            //    System.out.println(((Animal)aI).name + " je typu " + aI.getClass().getName() + " a dělá "+ aI.sound());
            // }
            // //System.out.print(((Animal)animalsInterf[i]).name + " " + animalsInterf[i].sound());      //přetypování pole typu interface na Animal a vypsání jména, lze vypsat také přes for s indexem...

            ISoundable[] smesZvukuI = { new Dog("Marcel"), new Dog("Azor"), new Dog("Speedy"), new Cat("Maggie"), new Cat("Micka") /*, new Turtle("Milfka") - nelze! */ , new Car("Ford") };
            for (ISoundable sI : smesZvukuI) {
                //System.out.println(((sI instanceof Animal) ? ((Animal)sI).name : ((Car)sI).name) + " je typu " + sI.getClass().getName() + " a dělá " + sI.sound());
                //System.out.println(sI.getClass().getName() + " " + sI.getName() + " vydává zvuk " + sI.sound()); //1) vyvolání getName()...
                //System.out.println(sI.getName() + " vydává zvuk " + sI.sound());  //2) vyvolání vytvořené vlastnosti Name...
                System.out.println(sI.toString()); //3) překrytí a zavolání ToString(), aby byl výpis "celou větou"...
            }
            //System.out.println(((Turtle)animals[5]).getName());    //test vypsání jméno želvy u přetypované buňky z horního pole...
            //System.out.println("Počet zvířat je: " + Animal.countOfAnimals);
        }
}

interface ISoundable {
    String sound();

    String getName();
}

abstract class Animal {
    public String name;
    public static int countOfAnimals;

    // public Animal() { countOfAnimals++; }
    public Animal(String name) {
        this.name = name;
        countOfAnimals++;
    }
}

class Dog extends Animal implements ISoundable {
    // public bool isPedigree; //s rodokmenem...
    public Dog(String name) {
        super(name);
        // this.isPedigree = isPedigree;
    }

    public String sound() {
        return "haf";
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "Pes " + name + " vydává zvuk " + sound();
    }
}

class Cat extends Animal implements ISoundable {
    // public bool isPedigree; //s rodokmenem...
    public Cat(String name) {
        super(name);
        // this.isPedigree = isPedigree;
    }

    public String sound() {
        return "mňau";
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "Kočka " + name + " vydává zvuk " + sound();
    }
}

class Turtle extends Animal {
    // public int speed;
    public Turtle(String name) {
        super(name);
        // this.speed = speed;
    }

    public String getName() {
        return name;
    }
}

class Car implements ISoundable {
    public String name;
    // public int speed;

    public Car(String name) {
        this.name = name;
    }

    public String sound() {
        return "brm brm";
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "Auto " + name + " vydává zvuk " + sound();
    }

}