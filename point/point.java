package point;

import java.util.*;

interface IGeometry {
    double perimeter();

    double area();
}

class Person implements IGeometry {
    private double weight, height;

    public Person(double weight, double height) {
        this.weight = weight;
        this.height = height;
    }

    public double perimeter() {
        return (height / weight) * 2.54;
    }

    public double area() {
        return weight / Math.pow(height, 2);
    }
}

class Point {
    private double x, y;

    public double GetX() {
        return x;
    }

    public void SetX(double value) {
        x = value;
    }

    public double GetY() {
        return y;
    }

    public void SetY(double value) {
        y = value;
    }

    public Point(double x, double y) {
        this.SetX(x);
        this.SetY(y);
    }

    public String toString() {
        return "(" + Math.round(GetX() * 100) / 100 + ";" + Math.round(GetY() * 100) / 100 + ")"; // popř. použít :f na zaokrouhlení namísto Math.Round(), popř. enum RoundingMode.HALF_UP
    }
}

class Shape { // měla by být abstraktní (i v reálném světě je tvar abstraktní), jednotlivé instance třídy by tedy nešly, pouze pole...
    private Point center; // skládání tříd s Point->Shape...

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Shape(Point center) {
        // this(center.GetX(), center.GetY());
        this.setCenter(center);
    }

    public Shape(double x, double y) {
        this(new Point(x, y));
        // center.SetX(x);
        // center.SetY(y);
    }

    public String toString() {
        return "Tvar má střed v bodě: " + getCenter().toString();
    }

    public void writeInfo() {
        System.out.println(this.toString());
    }

    // public abstract double perimeter();
    // public abstract double area();
}

class Circle extends Shape implements IGeometry { // tip: stačí když implementace interfacu bude jen u nadřízené třídy, pokud vybrané metody/složky taky aplikuje...
    public int r;

    public Circle(Point center, int r) {
        super(center);
        this.r = r;
    }

    public Circle(int r) {
        this(new Point(0, 0), r);
        this.r = r;
    }

    public Circle(double x, double y, int r) {
        super(new Point(x, y));
        this.r = r;
    }

    public String toString() {
        return "Kruh má poloměr " + r + " a střed v bodě " + getCenter().toString(); // zavoláním getCenter.toString() se zobrazí formát toString() třídy Point...
    }

    public void writeInfo() {
        System.out.println(this.toString() + " obvod: " + this.perimeter() + ", plocha: " + this.area());
    }

    public double perimeter() {
        return 2 * Math.PI * r;
    }

    public double area() {
        return Math.round((r * r * Math.PI) * 100) / 100;
    }
}

class Rectangle extends Shape implements IGeometry {
    public int a, b;

    public Rectangle(Point center, int a, int b) {
        super(center);
        this.a = a;
        this.b = b;
    }

    public Rectangle(int a, int b) {
        this(new Point(0, 0), a, b);
        this.a = a;
        this.b = b;
    }

    public Rectangle(Point center, int a) {
        super(center); // popř. this(center, a, a)
        this.a = this.b = a;
        // this.b = 0;
    }

    public Rectangle(int a) {
        this(new Point(0, 0), a); // popř. this(new Point(0, 0), a, a)
        this.a = this.b = a;
        // this.b = 0;
    }

    public String toString() {
        if (a != b) {
            return "Obdelník má stranu A o délce" + a + ", stranu B o délce " + b + " a střed v bodě "
                    + getCenter().toString();
        } else {
            return "Čtverec má stranu A o délce " + a + " a střed v bodě " + getCenter().toString();
        }
    }

    public void writeInfo() {
        System.out.println(this.toString() + " obvod: " + this.perimeter() + ", plocha: " + this.area());
    }

    public double perimeter() {
        if (a != b) {
            return 2 * a + 2 * b; // stačil by jen tenhle řádek, protože mám v konstruktorech nastaveno (this.a = this.b = a)
        } else {
            return 4 * a;
        }
    }

    public double area() {
        if (a != b) {
            return a * b; // stačil by jen tenhle řádek, protože mám v konstruktorech nastaveno (this.a = this.b = a)
        } else {
            return a * a;
        }
    }
}

class Cylinder extends Circle {
    // private Circle baseCircle; //podstava (kruh)
    private double height;

    public Cylinder(int r, double height) {
        super(r);
        // this.baseCircle = baseCircle;
        this.r = r;
        this.height = height;
    }

    // popř.:
    public Cylinder(Circle baseCircle, double height) {
        super(baseCircle.getCenter().GetX(), baseCircle.getCenter().GetY(), baseCircle.r); // super musí být ve správném pořadí dle výchozího konstruktoru...
        // this.baseCircle = baseCircle;
        this.height = height;
    }

    public double surface() { // povrch
        return (2 * super.area() + super.perimeter() * height);
    }

    public double volume() { // objem
        return super.area() * height;
    }

    public String toString() {
        return "Válec má podstavu " + "[" + super.toString() + "]"; // "a střed v bodě super.getCenter().toString()" je již obsahem super.toString()...
    }

    public void writeInfo() {
        System.out.println(toString() + " povrch: " + surface() + ", objem: " + volume());
    }

}

class TestPoint {
    public static void main(String[] args) {
        Point p1 = new Point(3.456, 2.765);
        Point p2 = new Point(3.123, 4.098);
        Point p3 = new Point(7.321, 9.45);
        Shape s1 = new Shape(p2);
        Shape s2 = new Shape(5.23, 6.432);
        Circle c1 = new Circle(p1, 4);
        Circle c2 = new Circle(3);
        Circle c3 = new Circle(4.5, 2.1, 10);
        Rectangle r1 = new Rectangle(p3, 7, 2);
        Rectangle r2 = new Rectangle(14, 9);
        Rectangle r3 = new Rectangle(p3, 10);
        Rectangle r4 = new Rectangle(5);

        Person pers1 = new Person(80.2, 75.4);
        Person pers2 = new Person(92.5, 62.7);
        Person pers3 = new Person(120.6, 94.5);

        Cylinder cyl1 = new Cylinder(53, 78.6);
        Cylinder cyl2 = new Cylinder(c1, 78.6);
        System.out.println(cyl1);
        cyl1.writeInfo();
        System.out.println(cyl2);
        cyl2.writeInfo();

        

        double totalPerimetr;

        List<IGeometry> geometryObjectsList = Arrays.asList(c1, c2, c3, r1, r2, r3, r4, pers1, pers2, pers3);        
        totalPerimetr = 0;
        for (IGeometry g : geometryObjectsList) {
        totalPerimetr += g.perimeter();
        }
        System.out.println("Součet obvodů všech geometrických objektů v kolekci (List): " + totalPerimetr);

        ArrayList<Object> geometryObjectsArrayList = new ArrayList<>();
        geometryObjectsArrayList.add(c1);
        geometryObjectsArrayList.add(c2);
        geometryObjectsArrayList.add(c3);
        geometryObjectsArrayList.add(r1);
        geometryObjectsArrayList.add(r2);
        geometryObjectsArrayList.add(r3);
        geometryObjectsArrayList.add(r4);
        geometryObjectsArrayList.add(pers1);
        geometryObjectsArrayList.add(pers2);
        geometryObjectsArrayList.add(pers3);
        totalPerimetr = 0;
        for (Object g : geometryObjectsArrayList) { //ArrayList umí jabka s hruškama...
        totalPerimetr += ((IGeometry)g).perimeter(); //zde pak jen přetypovat...
        }
        System.out.println("Součet obvodů všech geometrických objektů v kolekci (ArrayList): " + totalPerimetr);
    }
}
