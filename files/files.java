package files;

import java.io.*;

class Person implements Serializable {
    private static final long serialVersionUID = 1L;    //bez toho je u Person warning...
    
    public String name;
    public int age;
    // datové složky by měly být private a přistupovat k nim skrz properties, popř.
    // getters/setters...

    public Person() { }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

class FileSerial {
    public static void main(String[] args) {
        Person luke = new Person("Luke", 27);
        try {
            ObjectOutputStream vystup = new ObjectOutputStream(new FileOutputStream("soubor.txt"));
            vystup.writeObject(luke);
            vystup.close();
        } catch (IOException e) { System.out.println("Soubor nelze zapsat!"); }
        
        Person p1 = new Person();
        try {
            ObjectInputStream vstup = new ObjectInputStream(new FileInputStream("soubor.txt"));
            p1 = (Person) vstup.readObject();
            vstup.close();
        } catch (ClassNotFoundException e) { System.out.println("Třída nelze najít!"); }
          catch (FileNotFoundException e) { System.out.println("Soubor nelze najít!"); }
          catch (IOException e) { System.out.println("Soubor nelze číst!"); }
        
        System.out.println(p1.name + ", " + p1.age);
    }
}
