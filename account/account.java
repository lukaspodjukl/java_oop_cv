package account;

import java.util.Scanner;

class Account {
    public int balance = 0;
    public String name; // pro potřebu pojmenování účtů ve výpisech...

    public Account(String name) {
        this.name = name;
    }

    public void insertInto(int amount) throws MaloPenezException {
        if (amount == 0) {
            throw new MaloPenezException("Nelze vložit ani vybrat 0 Kč!");
        }
        if (amount > 0) { // vklad
            balance += amount;
            System.out.println("Proveden vklad " + amount + " Kč na účet " + name);
        } else if (amount < 0) { // výběr
            if (balance < Math.abs(amount)) {
                throw new MaloPenezException(
                        "Nelze provést výběr z účtu " + name + " nedostatečný zůstatek: " + balance + " Kč");
            } else {
                balance -= Math.abs(amount);
                System.out.println("Proveden výběr " + amount + " Kč z účtu " + name);
            }
        }
    }

    public void writeBalance() {
        System.out.println("Stav účtu " + name + ": " + balance + " Kč");
    }

    public void transferTo(Account another, int amount) throws MaloPenezException {
        if ((amount <= balance) && !(amount <= 0) && (balance != 0) && (another != this)) {
            balance -= amount;
            another.balance += amount;
            System.out.println("Částka " + amount + " Kč odeslána z účtu " + name + " na účet " + another.name);
        } else {
            if (amount < 0) {
                throw new MaloPenezException("Nesmíte vysávat cizí účty!");
            }
            if (amount == 0) {
                throw new MaloPenezException("Nemůžete převádět 0 Kč!");
            }
            if (amount > balance) {
                throw new MaloPenezException("Nemůžete převést více peněz než máte!");
            }
        }
    }

}

class MaloPenezException extends Throwable { // Exception...    
    private static final long serialVersionUID = 1L;    //bez toho se ukazoval warning...

    public MaloPenezException(String s) {
        super(s);
    }

    public MaloPenezException() {
        super();
    }

    // public String message = "Málo peněz!"; //funguje (veřejná datová složka
    // namísto vlastnosti) lze volat pro instnci, ale přecejen raději getter viz
    // níže...

    @Override
    public String getMessage() {
        return "Málo peněz! " + super.getMessage(); // nebo tohle zakomentovat a do catch v mainu dát rovnou: System.out.println("Málo peněz! " + e.getLocalizedMessage());
    }
}

class TestAccount {
    public static void main(String[] args) {
        Account u1 = new Account("u1");
        Account u2 = new Account("u2");
        Scanner sc = new Scanner(System.in); // možná by šlo pro int i tohle System.in.read(), ale Scanner je
                                             // univerzálnější...

        int howMuch;
        do {
            System.out.println("Zadejte částku pro vložení/výběr:");
            howMuch = sc.nextInt();
            try {
                u1.insertInto(howMuch);
                break;
            } catch (MaloPenezException e) {
                System.out.println(e.getMessage());
            }
        } while (true);

        boolean OK;
        do {
            OK = true;
            System.out.println("Zadejte částku pro vložení/výběr:");
            howMuch = sc.nextInt();
            try {
                u2.insertInto(howMuch);
            } catch (MaloPenezException e) {
                System.out.println(e.getMessage());
                OK = false;
            }
        } while (!OK);

        do {
            OK = true;
            System.out.println("Zadejte částku pro odeslání na účet:");
            howMuch = sc.nextInt();
            try {
                u1.transferTo(u2, howMuch);
            } catch (MaloPenezException e) {
                System.out.println(e.getMessage());
                OK = false;
            }
        } while (!OK && (u1.balance > 0)); // bez podmínky (u1.balance > 0) by byla nekonečná smyčka, kvůli vyvolání vyjímky a tedy změny OK...

        u1.writeBalance();
        u2.writeBalance();

        sc.close(); // Scanner by se měl nakonec uzavřít...
    }
}
